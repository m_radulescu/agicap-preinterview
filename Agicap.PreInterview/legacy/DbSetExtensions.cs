﻿using Microsoft.EntityFrameworkCore;

namespace Agicap.PreInterview
{
    public static class DbSetExtensions
    {
        [Obsolete("This method will be removed in a later version")]
        public static void Update(this DbSet<CarEntity> dbSet, long? id, string color, Model model)
        {
            var entity = dbSet.FirstOrDefault(e => e.Id.Equals(id));
            entity.Color = color;
            entity.Type = model;
            dbSet.Update(entity);
        }
    }
}
