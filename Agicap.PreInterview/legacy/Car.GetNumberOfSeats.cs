﻿namespace Agicap.PreInterview
{
    public partial class Car
    {
        [Obsolete("Method GetNumberOfSeats will be removed in later version. Use Model.GetNumberOfSeats()")]
        public static float GetNumberOfSeats(Model type) => Convert.ToSingle(type.GetNumberOfSeats());
    }
}
