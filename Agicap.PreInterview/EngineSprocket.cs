﻿namespace Agicap.PreInterview
{
    public class EngineSprocket
    {
        public void Prime() {}

        public bool Engage() => true;
    }
}
