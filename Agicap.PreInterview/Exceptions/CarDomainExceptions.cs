﻿using System.Runtime.Serialization;

namespace Agicap.PreInterview.Exceptions
{
    public class CarDomainException : Exception
    {
        public CarDomainException(){}
        public CarDomainException(string? message) : base(message) {}
        public CarDomainException(string? message, Exception? innerException) : base(message, innerException) {}
        protected CarDomainException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }

    public class CarCouldNotBeStartedException : CarDomainException
    {
        private const string DefaultMessage = "The engine could not be started";
        public CarCouldNotBeStartedException(string message = DefaultMessage) : base(message) { }
    }
}
