﻿using Agicap.PreInterview.SeedWork;

namespace Agicap.PreInterview.Infrastructure
{
    public interface ICarRepository : IRepository<long?, CarEntity>
    {
    }
}
