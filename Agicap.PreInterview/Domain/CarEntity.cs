﻿using Agicap.PreInterview.SeedWork;

namespace Agicap.PreInterview
{
    public class CarEntity : IAggregate<long?>
    {
        public long? Id { get; set; }
        public string Color { get; set; }
        public Model Type { get; set; }
        public long KilometersAmount { get; set; }
        public bool IsDeleted { get; set; }

        public CarEntity() { }

        [Obsolete("This ctor will be removed in later versions, avoid using it")]
        public CarEntity(Car car) 
        {
            Id = car.Id;
            Color = car.Color;
            Type = car.Type;
            KilometersAmount = car.KmAmount;
        }
    }
}
