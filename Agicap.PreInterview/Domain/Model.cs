﻿namespace Agicap.PreInterview
{
    public enum Model
    {
        QashqaiTekna,
        QashqaiVisia,
        QashqaiNConnecta,
        QashqaiAcenta,
        ScenicPhase1,
        ScenicPhase2,
        Peugeot208Phase1,
        Peugeot208Phase2
    }

    public static class ModelExtensions
    {
        public static int GetNumberOfSeats(this Model model)
            => model switch
            {
                Model.QashqaiAcenta or Model.QashqaiNConnecta => 4,
                Model.QashqaiTekna => 5,
                _ => 0
            };
        public static bool HasSunroof(this Model model)
            => model switch
            {
                Model.Peugeot208Phase1 or Model.QashqaiTekna or Model.QashqaiVisia or Model.QashqaiAcenta or Model.Peugeot208Phase2 => true,
                _ => false
            };
    }
}