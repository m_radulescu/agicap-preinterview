﻿using Agicap.PreInterview.Infrastructure;

namespace Agicap.PreInterview.Domain
{
    public interface ICarUnitOfWork
    {
        public ICarRepository Cars { get; }
    }
}
