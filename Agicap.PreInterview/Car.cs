﻿using Agicap.PreInterview.Domain;
using Agicap.PreInterview.Exceptions;
using Agicap.PreInterview.Infrastructure;

namespace Agicap.PreInterview
{
    public partial class Car
    {
        private readonly ICarUnitOfWork _unitOfWork;
        private readonly EngineSprocket _engineSprocket;
        private readonly CarEntity _state = new();

        public long? Id { get => _state.Id; set => _state.Id = value; }
        public string Color { get => _state.Color; set => _state.Color = value; }
        public Model Type { get => _state.Type; set => _state.Type = value; }
        public int NumberOfSeats => Type.GetNumberOfSeats();
        public long KmAmount { get => _state.KilometersAmount; set => _state.KilometersAmount = value; }

        [Obsolete("this constructor will be removed in a later version, to facilitate constructor with dependency injection")]
        public Car(long? id, string color, Model type, long kmAmount) : this(new EFCarUnitOfWork(new DbContext("myConnectionString")), new EngineSprocket())
        {
            Id = id;
            Color = color;
            Type = type;
            KmAmount = kmAmount;
        }
        public Car(ICarUnitOfWork unitOfWork, EngineSprocket engineSprocket) : this(null, unitOfWork, engineSprocket) { }
        private Car(CarEntity? initialState, ICarUnitOfWork unitOfWork, EngineSprocket engineSprocket)
        {
            _state = initialState ?? _state;
            _unitOfWork = unitOfWork;
            _engineSprocket = engineSprocket;
        }

        public string GetCurrentModel() => $"This car is a {Type}";

        public bool HasToitOuvrant() => Type.HasSunroof();

        public void Save()
        {
            var existingState = _unitOfWork.Cars.Get(Id);
            _state.KilometersAmount = existingState?.KilometersAmount ?? _state.KilometersAmount;
            _unitOfWork.Cars.Add(_state);
        }

        public async Task<Car> Get(long id) => new Car(await _unitOfWork.Cars.GetAsync(id), _unitOfWork, _engineSprocket);

        public async Task<IEnumerable<Car>> GetAll() => (await _unitOfWork.Cars.GetAllAsync()).Select(e => new Car(e, _unitOfWork, _engineSprocket));

        [Obsolete("Method GetCarAndUpdateKms will be removed in a later version. Use Get(long id) followed by IncrementKmAmount()")]
        public async Task<Car> GetCarAndUpdateKms() => await (await Get(Id.Value)).IncrementKmAmount();

        public async Task<Car> IncrementKmAmount() 
        { 
            _state.KilometersAmount++;
            await _unitOfWork.Cars.AddAsync(_state);
            return this;
        }

        public bool Start()
        {
            try
            {
                _engineSprocket.Prime();
                return _engineSprocket.Engage();
            }
            catch (Exception)
            {
                throw new CarCouldNotBeStartedException();
            }
        }
    }
}