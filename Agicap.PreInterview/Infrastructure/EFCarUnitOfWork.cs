﻿using Agicap.PreInterview.Domain;
using Agicap.PreInterview.SeedWork.EntityFramework;

namespace Agicap.PreInterview.Infrastructure
{
    public class EFCarUnitOfWork : EFUnitOfWork, ICarUnitOfWork
    {
        public EFCarUnitOfWork(DbContext context) : base(context)
        {
            Cars = new EFCarRepository(Context);
        }

        public ICarRepository Cars { get; init; }
    }
}
