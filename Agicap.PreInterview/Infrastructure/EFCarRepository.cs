﻿using Agicap.PreInterview.SeedWork.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Agicap.PreInterview.Infrastructure
{
    public class EFCarRepository : EFRepository<long?, CarEntity>, ICarRepository
    {
        public EFCarRepository(Microsoft.EntityFrameworkCore.DbContext context) : base(context) {}

        public override IEnumerable<CarEntity> GetAll() => DbSet.Where(e => !e.IsDeleted);
        public override async Task<IEnumerable<CarEntity>> GetAllAsync(CancellationToken cancellationToken = default) => (await DbSet.Where(e => !e.IsDeleted).ToListAsync(cancellationToken)).AsEnumerable();
    }
}
