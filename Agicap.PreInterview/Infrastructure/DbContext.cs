﻿using Microsoft.EntityFrameworkCore;

namespace Agicap.PreInterview
{
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private string _connection;

        public virtual DbSet<CarEntity> Cars { get; }

        public DbContext(string connection) => _connection = connection;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connection);
        }
    }
}
