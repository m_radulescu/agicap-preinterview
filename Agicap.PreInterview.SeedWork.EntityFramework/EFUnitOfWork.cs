﻿using Microsoft.EntityFrameworkCore;

namespace Agicap.PreInterview.SeedWork.EntityFramework
{
    public abstract class EFUnitOfWork : IUnitOfWork
    {
        protected DbContext Context { get; init;  }

        public EFUnitOfWork(DbContext context)
        { 
            Context = context;
        }
    }
}
