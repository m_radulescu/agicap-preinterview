﻿using Microsoft.EntityFrameworkCore;

namespace Agicap.PreInterview.SeedWork.EntityFramework
{
    public class EFRepository<TId, TAggregate> : IRepository<TId, TAggregate>
        where TAggregate : class, IAggregate<TId>, new()
    {
        protected DbContext Context { get; init; }
        protected DbSet<TAggregate> DbSet { get => Context.Set<TAggregate>();}


        public EFRepository(DbContext context)
        {
            Context = context;
        }

        public virtual void Add(TAggregate aggregate)
        {
            if (aggregate.Id != null && !DbSet.Any(x => x.Id.Equals(aggregate.Id)))
            {
                DbSet.Add(aggregate);
                Context.SaveChanges();
                return;
            }
            DbSet.Update(aggregate);
            Context.SaveChanges();
        }

        public virtual TAggregate? Get(TId id) => DbSet.FirstOrDefault(e => e.Id.Equals(id));

        public virtual IEnumerable<TAggregate> GetAll() => DbSet.AsEnumerable();

        public virtual async Task AddAsync(TAggregate aggregate, CancellationToken cancellationToken = default)
        {
            if (aggregate.Id != null && !DbSet.Any(x => x.Id.Equals(aggregate.Id)))
            {
                await DbSet.AddAsync(aggregate, cancellationToken);
                await Context.SaveChangesAsync(cancellationToken);
                return;
            }
            DbSet.Update(aggregate);
            await Context.SaveChangesAsync(cancellationToken);
        }

        public virtual Task<TAggregate?> GetAsync(TId id, CancellationToken cancellationToken = default) => DbSet.FirstOrDefaultAsync(e => e.Id.Equals(id));

        public virtual async Task<IEnumerable<TAggregate>> GetAllAsync(CancellationToken cancellationToken = default) 
        {
            var aggregates = await DbSet.ToListAsync(cancellationToken);
            return aggregates.AsEnumerable();
        }
    }
}
