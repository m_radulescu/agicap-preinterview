using Agicap.PreInterview.Domain;
using Agicap.PreInterview.Infrastructure;
using EntityFrameworkCoreMock.NSubstitute;
using FluentAssertions;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Agicap.PreInterview.Tests
{
    public class CarTests
    {
        [Fact]
        public void Save_ShouldCreateNewCar_WhenValidParametersAndCarDoesNotExist()
        {
            var kmAmount = 10;
            var color = "red";
            var type = Model.ScenicPhase2;

            var (car, dbContext, _) = CreateCar(new List<CarEntity>());
            car.KmAmount = kmAmount;
            car.Color = color;
            car.Type = type;
            car.Save();

            var result = dbContext.Cars.FirstOrDefault();
            result.Should().NotBeNull();
            result.Id.Should().BeNull();
            result.Color.Should().Be(color);
            result.KilometersAmount.Should().Be(kmAmount);
            result.Type.Should().Be(type);
        }

        [Fact]
        public void Save_ShouldNotUpdateKmAmmount_WhenCarExist()
        {
            var id = 1;
            var kmAmmount = 200;
            var color = "blue";
            var type = Model.ScenicPhase2;

            var initialEntity = new CarEntity
            {
                Id = id,
                Color = "red",
                IsDeleted = false,
                KilometersAmount = 10,
                Type = Model.ScenicPhase1
            };

            var (car, dbContext, _) = CreateCar(new List<CarEntity>() { initialEntity });
            car.Id = id;
            car.KmAmount = kmAmmount;
            car.Color = "blue";
            car.Type = Model.ScenicPhase2;

            car.Save();

            var result = dbContext.Cars.FirstOrDefault();
            result.Id.Should().Be(initialEntity.Id);
            result.Color.Should().Be(color);
            result.KilometersAmount.Should().Be(initialEntity.KilometersAmount);
            result.Type.Should().Be(type);
        }

        [Fact]
        public async Task Get_ShuldReturnCar_WhenExists()
        {
            var expectedCar = new CarEntity()
            {
                Id = 10,
                Color = "red",
                KilometersAmount = 100,
                Type = Model.ScenicPhase1
            };
            var (car, _, _) = CreateCar(new List<CarEntity>() { expectedCar });

            var result = await car.Get(expectedCar.Id.Value);
            result.Id.Should().Be(expectedCar.Id);
            result.Color.Should().Be(expectedCar.Color);
            result.KmAmount.Should().Be(expectedCar.KilometersAmount);
            result.Type.Should().Be(expectedCar.Type);
        }

        [Fact]
        public async Task GetAll_ShuldReturnAllUndeletedCars()
        {
            var (car, _, _) = CreateCar(new()
            {
                new() { Id = 1 },
                new() { Id = 2, IsDeleted = true },
                new() { Id = 3, IsDeleted = false },
            });
            var cars = (await car.GetAll()).ToList();
            cars.Count().Should().Be(2);
            cars[0].Id.Should().Be(1);
            cars[1].Id.Should().Be(3);
        }

        [Fact]
        public async Task GetCurrentModel_ShouldPass()
        {
            var expected = "This car is a QashqaiTekna";
            (Car car, _, _) = CreateCar(new());
            car.Type = Model.QashqaiTekna;
            car.GetCurrentModel().Should().Be(expected);
        }

        /// <summary>
        /// This test is just a deduction, that this method should do this 
        /// IRL this method does not update the kmAmount in any shape or form and just returns a new Car class with the updated kmAmount
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetCarAndUpdateKms_ShouldIncrementOnlyKmAmmount()
        {
            var id = 1;
            var color = "red";
            var kmAmount = 100;
            var type = Model.ScenicPhase1;
            var (car, dbContext, _) = CreateCar(new() {
                new()
                {
                    Id = id,
                    Color = color,
                    KilometersAmount = kmAmount,
                    Type = type
                }
            });
            car.Id = id;

            await car.GetCarAndUpdateKms();
            var result = dbContext.Cars.FirstOrDefault();
            result.Id.Should().Be(id);
            result.Color.Should().Be(color);
            result.KilometersAmount.Should().Be(kmAmount + 1);
            result.Type.Should().Be(type);
        }

        [Fact]
        public void HasToitOuvrant_ShouldPass()
        {
            (Car car, DbContext dbContext, EngineSprocket engine) = CreateCar(new());
            car.Type = Model.Peugeot208Phase1;
            car.HasToitOuvrant().Should().BeTrue();
            car.Type = Model.QashqaiTekna;
            car.HasToitOuvrant().Should().BeTrue();
            car.Type = Model.QashqaiVisia;
            car.HasToitOuvrant().Should().BeTrue();
            car.Type = Model.QashqaiAcenta;
            car.HasToitOuvrant().Should().BeTrue();
            car.Type = Model.Peugeot208Phase2;
            car.HasToitOuvrant().Should().BeTrue();

            car.Type = Model.QashqaiNConnecta;
            car.HasToitOuvrant().Should().BeFalse();
            car.Type = Model.ScenicPhase1;
            car.HasToitOuvrant().Should().BeFalse();
            car.Type = Model.ScenicPhase2;
            car.HasToitOuvrant().Should().BeFalse();
        }

        [Fact]
        public void GetNumberOfSeats_ShouldPass() 
        {
            Car.GetNumberOfSeats(Model.QashqaiAcenta).Should().Be(4);
            Car.GetNumberOfSeats(Model.QashqaiNConnecta).Should().Be(4);
            Car.GetNumberOfSeats(Model.QashqaiTekna).Should().Be(5);
        }

        [Fact]
        public void Start_ShouldPass() 
        {
            var (car, _, engine) = CreateCar(new());
            car.Start();
            engine.Received().Prime();
            engine.Received().Engage();
        }



        public static (Car car, DbContext dbContext, EngineSprocket engine) CreateCar(List<CarEntity> carEntities) 
        {
            var engine = Substitute.For<EngineSprocket>();
            var dbContext = SubstituteCarDbContext(carEntities);
            var uow = Substitute.For<EFCarUnitOfWork>(dbContext);
            var car = new Car(uow, engine);
            return (car, dbContext, engine);
        }
        public static DbContext SubstituteCarDbContext(List<CarEntity> carEntities)
        {
            var dbContextMock = new DbContextMock<DbContext>("");
            var usersDbSetMock = dbContextMock.CreateDbSetMock(x => x.Cars, carEntities);
            return dbContextMock.Object;
        }
    }
}