﻿namespace Agicap.PreInterview.SeedWork
{
    public interface IRepository<TId, TAggregate>
        where TAggregate : class, IAggregate<TId>, new()
    {
        /// <summary>
        /// Based on the Repository pattern there is no "Save"/"Update"/"Create" methods
        /// This method should handle both actions Update and Create 
        /// </summary>
        /// <param name="aggregate"></param>
        void Add(TAggregate aggregate);
        TAggregate? Get(TId id);
        IEnumerable<TAggregate> GetAll();

        Task AddAsync(TAggregate aggregate, CancellationToken cancellationToken = default);
        Task<TAggregate?> GetAsync(TId id, CancellationToken cancellationToken = default);
        Task<IEnumerable<TAggregate>> GetAllAsync(CancellationToken cancellationToken = default);
    }
}