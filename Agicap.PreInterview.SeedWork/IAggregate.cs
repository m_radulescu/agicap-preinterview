﻿namespace Agicap.PreInterview.SeedWork
{
    public interface IAggregate<TId>
    {
        TId Id { get; }
    }
}
